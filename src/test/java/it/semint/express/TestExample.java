package it.semint.express;

import org.junit.Before;
import org.junit.Test;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.*;
import uk.ac.manchester.cs.jfact.JFactFactory;

import java.util.Set;

import static org.junit.Assert.assertTrue;

public class TestExample {
    OWLReasonerFactory reasonerFactory = null;
    OWLOntology pizzaOntology;

    @Before
    public void setUp() throws OWLOntologyCreationException {
        System.out.println("SETUP TestExample");
        this.reasonerFactory = new JFactFactory();
        this.pizzaOntology = OWLManager.createOWLOntologyManager().loadOntologyFromOntologyDocument(
                TestExample.class.getResourceAsStream("/pizza.owl"));
        // or from IRI
        //        this.pizzaOntology = OWLManager.createOWLOntologyManager().loadOntology(
        //                IRI.create("https://protege.stanford.edu/ontologies/pizza/pizza.owl"));

    }

    @Test
    public void testReasoner() throws Exception {

        // a config object. Things like monitor, timeout, etc, go here
        OWLReasonerConfiguration config = new SimpleConfiguration(50000);

        // Create a reasoner that will reason over our ontology and its imports
        // closure. Pass in the configuration.
        OWLReasoner reasoner = this.reasonerFactory.createReasoner(this.pizzaOntology, config);

        // Ask the reasoner to classify the ontology
        reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);

        checkConsistency(reasoner);

        listUnsatisfiable(reasoner);

        listSubclasses(reasoner);

        listInstances(reasoner);

    }

    private void checkConsistency(OWLReasoner reasoner) {
        // We can determine if the ontology is actually consistent (in this
        // case, it should be).
        assertTrue(reasoner.isConsistent());
        if (reasoner.isConsistent()) {
            System.out.println("---------------------");
            System.out.println(" CONSISTENT!");
        }
    }

    private void listUnsatisfiable(OWLReasoner reasoner) {
        // get a list of unsatisfiable classes
        Node<OWLClass> bottomNode = reasoner.getUnsatisfiableClasses();
        // leave owl:Nothing out
        System.out.println("---------------------");
        Set<OWLClass> unsatisfiable = bottomNode.getEntitiesMinusBottom();
        if (!unsatisfiable.isEmpty()) {
            System.out.println("The following classes are unsatisfiable: ");
            for (OWLClass cls : unsatisfiable) {
                System.out.println(" -- "+cls.getIRI().getFragment());
            }
        } else {
            System.out.println("There are no unsatisfiable classes");
        }
    }

    private void listSubclasses(OWLReasoner reasoner) {
        // Look up and print all direct subclasses for all classes
        System.out.println("---------------------");
        System.out.println(" SUBCLASSES:");
        for (OWLClass c : this.pizzaOntology.getClassesInSignature()) {
            // the boolean argument specifies direct subclasses; false would
            // specify all subclasses
            // a NodeSet represents a set of Nodes.
            // a Node represents a set of equivalent classes
            NodeSet<OWLClass> subClasses = reasoner.getSubClasses(c, true);
            for (OWLClass subClass : subClasses.getFlattened()) {
                System.out.println(subClass.getIRI().getFragment() + "\tsubclass of\t"
                        + c.getIRI().getFragment());
            }
        }
    }

    private void listInstances(OWLReasoner reasoner) {
        // for each class, look up the instances
        System.out.println("---------------------");
        System.out.println(" INSTANCES:");
        for (OWLClass c : this.pizzaOntology.getClassesInSignature()) {
            // the boolean argument specifies direct subclasses; false would
            // specify all subclasses
            // a NodeSet represents a set of Nodes.
            // a Node represents a set of equivalent classes/or sameAs
            // individuals
            NodeSet<OWLNamedIndividual> instances = reasoner.getInstances(c, true);
            for (OWLNamedIndividual i : instances.getFlattened()) {
                System.out.println(i.getIRI().getFragment() + "\tinstance of\t"
                        + c.getIRI().getFragment());
                // look up all property assertions
                for (OWLObjectProperty op : this.pizzaOntology
                        .getObjectPropertiesInSignature()) {
                    NodeSet<OWLNamedIndividual> petValuesNodeSet = reasoner
                            .getObjectPropertyValues(i, op);
                    for (OWLNamedIndividual value : petValuesNodeSet.getFlattened()) {
                        System.out.println(i.getIRI().getFragment() + "\t"
                                + op.getIRI().getFragment() + "\t"
                                + value.getIRI().getFragment());
                    }
                }
            }
        }
    }
}